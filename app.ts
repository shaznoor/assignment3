/*Questions*/
let questions = [
    {
    numb: 1,
    question: "Which feature of OOP indicates code reusability?",
    answer: '2',
    options: [
      "Encapsulation",
      "Inheritance",
      "Abstraction",
      "Polymorphism"
    ]
  },
  {
    numb: 2,
    question: "Which of the following is not a feature of pure OOP?",
    answer: '3',
    options: [
      "Classes must be used",
      "Inheritance",
      "Data may/may not be declared using object",
      "Functions Overloading"
    ]
  },
  {
    numb: 3,
    question: "Which among the following doesn’t come under OOP concept?",
    answer: '1',
    options: [
      "Platform independent",
      "Data binding",
      "Message passing",
      "Data hiding"
    ]
  },
  {
    numb: 4,
    question: "Which feature may be violated if we don’t use classes in a program?",
    answer: '4',
    options: [
      "Inheritance can’t be implemented",
      "Object must be used is violated",
      "Encapsulation only is violated",
      "Basically all the features of OOP gets violated"
    ]
  },
  {
    numb: 5,
    question: "Which among doesn’t illustrates polymorphism?",
    answer: '2',
    options: [
      "Function overloading",
      "Function overriding",
      "Operator overloading",
      "Virtual function"
    ]
  },
  {
    numb: 6,
    question: "Which among the following, for a pure OOP language, is true?",
    answer: '4',
    options: [
      "The language should follow 3 or more features of OOP",
      "The language should follow at least 1 feature of OOP",
      "The language must follow only 3 features of OOP",
      "The language must follow all the rules of OOP"
    ]
  },
  {
    numb: 7,
    question: "Does OOP provide better security than POP?",
    answer: '1',
    options: [
      "Always true for any programming language",
      "May not be true with respect to all programming languages",
      "It depends on type of program",
      "It’s vice-versa is true"
    ]
  },
  {
    numb: 8,
    question: "Which of the following is not type of class?",
    answer: '3',
    options: [
      "Abstract Class",
      "Final Class",
      "Start Class",
      "String Class"
    ]
  },
  {
    numb: 9,
    question: "Which is known as a generic class?",
    answer: '3',
    options: [
      "Abstract class",
      "Final class",
      "Template class",
      "Efficient Code"
    ]
  },
  {
    numb: 10,
    question: "What is the scope of a class nested inside another class?",
    answer: '4',
    options: [
      "Protected scope",
      "Private scope",
      "Global scopev",
      "Depends on access specifier and inheritance used"
    ]
  }];

let result: number[]=[0,0,0,0,0,0,0,0,0,0];
let curr_que: number=0;
let remaining_time=300;

/*timer*/
let reduce:any=null;
let timer_min:HTMLElement=document.getElementById('min');
let timer_sec:HTMLElement=document.getElementById('sec');
function timer(){
  reduce=setInterval(()=>{
    remaining_time-=1;
    let minutes:number = Math.floor(remaining_time / 60);
    let seconds:number = remaining_time - minutes * 60;
    timer_min.innerHTML=minutes.toString();
    timer_sec.innerHTML=seconds.toString();
    if(remaining_time<=0){
      let finish_btn:HTMLElement=document.getElementById('finish_modal');
      finish_btn.click();
    }
  },1000)
}

/*start btn*/
let start_btn: HTMLElement=document.getElementById('start_btn');
start_btn.addEventListener('click',()=>{
    let info_modal:HTMLElement=document.getElementById('info_modal');
    let quiz_modal:HTMLElement=document.getElementById('quiz_modal');
    info_modal.style.opacity='0';
    info_modal.style.pointerEvents='none';
    quiz_modal.style.opacity='1';
    quiz_modal.style.pointerEvents='all';
    let question_text:HTMLElement=document.getElementById('question_text');
    let option1:HTMLElement=document.getElementById('option1');
    let option2:HTMLElement=document.getElementById('option2');
    let option3:HTMLElement=document.getElementById('option3');
    let option4:HTMLElement=document.getElementById('option4');
    const q1=questions[0];
    question_text.innerHTML=q1.question;
    option1.innerHTML=q1.options[0];
    option2.innerHTML=q1.options[1];
    option3.innerHTML=q1.options[2];
    option4.innerHTML=q1.options[3];
    curr_que=q1.numb;
    remaining_time=300;
    timer();
});

/*save btn check*/
let submit_ans:HTMLElement=document.getElementById('submit_modal');
submit_ans.addEventListener('click',()=>{
    let a:NodeListOf<HTMLElement>=document.getElementsByName('option');
    let checkedAns: string='-1';
    for(let i:number=0;i<a.length;i++){
        if(a[i].checked){
            checkedAns=a[i].value.toString();
        }
    }
    if(checkedAns!='-1' && questions[curr_que-1].answer==checkedAns){
        result[curr_que-1]=1;
    } else {
        result[curr_que-1]=0;
    }
});


/*next btn*/
let next_modal:HTMLElement=document.getElementById('next_modal');
next_modal.addEventListener('click',()=>{
    let question_text:HTMLElement=document.getElementById('question_text');
    let option1:HTMLElement=document.getElementById('option1');
    let option2:HTMLElement=document.getElementById('option2');
    let option3:HTMLElement=document.getElementById('option3');
    let option4:HTMLElement=document.getElementById('option4');
    let a:NodeListOf<HTMLElement>=document.getElementsByName('option');
    const q1=questions[curr_que];
    question_text.innerHTML=q1.question;
    option1.innerHTML=q1.options[0];
    option2.innerHTML=q1.options[1];
    option3.innerHTML=q1.options[2];
    option4.innerHTML=q1.options[3];
    curr_que=q1.numb;
    for(let i:number=0;i<a.length;i++){
      a[i].checked=false;
    }
    const number_change:HTMLElement=document.getElementById('number_change');
    number_change.innerHTML=q1.numb.toString();
    if(curr_que==10){
        next_modal.setAttribute('disabled','disabled');
    }
});

/*finish_btn*/
let finish_btn:HTMLElement=document.getElementById('finish_modal');
finish_btn.addEventListener('click',()=>{
  let sum:number=0;
  let quiz_modal:HTMLElement=document.getElementById('quiz_modal');
  quiz_modal.style.opacity='0';
  quiz_modal.style.pointerEvents='none';
  for(let i:number=0;i<result.length;i++){
    sum+=result[i];
  }
  clearInterval(reduce);
  let total_sum:HTMLElement=document.getElementById('total_sum');
  total_sum.innerHTML=sum.toString();
  let result_box:HTMLElement=document.getElementById('result_box');
  result_box.style.opacity='1';
  result_box.style.pointerEvents='all';
});

/*restart btn*/
let restart_btn:HTMLElement=document.getElementById('restart');
restart_btn.addEventListener('click',()=>{
  location.reload();
});